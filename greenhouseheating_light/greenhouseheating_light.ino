// Greenhouse heating system - Light version
#include <Arduino.h>
#include <Thread.h>                    // ArduinoThread
#include <DHT.h>                       // DHT Sensor Library by Adafruit 1.2.3 (!!! 1.3 not working)

const byte relay = 2;                  // Relay PIN
#define DHTPIN 3                       // PIN DHT

DHT dht(DHTPIN, DHT11);                // DHT-22 - AM2302 init

int minTemp = 11;                      // Minimum temperature for relay (on)
int led = 13;
int maxTemp = 14;                      // Maximum temperature for relay (off)
long relayWorkingTime = 600000;        // 10min - minimun working time

int tempThreadMs = 10000;              // checkTemp thread interval
int buttonThreadMs = 200;              // pressButton thread interval
int buttonState = 0;                   // Button state
int buttonView = 0;                    // Button viewing state
int relayState = 0;                    // Relay state
long previousRelayChanged = 0;         // Last time relay state changed
long buttonPressed = 0;                // Button ms pressed counter
long previousButtonMillis = 0;         // Last time button pressing
long buttonInterval = 10000;           // Interval for highlighting 4-Digit Display after pressing button

// Threads:
Thread checkTempThread = Thread();     // Create thread for temperature state checking

void setup() {
  Serial.begin(9600);                  // Initializes the Serial connection @ 9600 baud for debug
  serStr("starting setup...");
  while (!Serial);                     // Wait until Arduino Serial Monitor opens
  dht.begin();
  pinMode(led, OUTPUT);     
  pinMode(relay, OUTPUT);
  digitalWrite(relay, HIGH);
  // Button state cheking thread:
  checkTempThread.onRun(checkTemp);
  checkTempThread.setInterval(tempThreadMs); // Interval for checking temperature
  delay(5000);                         // 5 second delay before display off and setup complete
  serStr("...setup finished");
}

void loop() {
  // Threads init:
  if (checkTempThread.shouldRun())
    checkTempThread.run();
  // Check RTC.
}

// Check temperature thread
void checkTemp() {
  unsigned long currentMillis = millis();
  if (((currentMillis - previousRelayChanged) > relayWorkingTime) || (previousRelayChanged == 0)) {
    float tempFloat;
    readDHTtemp(&tempFloat);
    if (tempFloat != tempFloat) {
      serStr("TEMPERATURE is Not A Number");
      for (int i = 0; i < 23; i++) {
        digitalWrite(relay, HIGH);
        digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(333);               // wait for a second
        digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
        delay(444);     
      }
    } else {
      int tempInt = (int)tempFloat;
      if ((tempInt < minTemp) && (relayState == 0)) {
        serStr("low temperature - heating on");
        digitalWrite(relay, LOW);
        previousRelayChanged = currentMillis;
        relayState = 1;
      }
      if ((tempInt > maxTemp) && (relayState == 1)) {
        serStr("high temperature - heating off");
        digitalWrite(relay, HIGH);
        previousRelayChanged = currentMillis;
        relayState = 0;
      }
    }
  }
  String string = "";
  String stringToPrint = "";
  float tempFloat;
  float humFloat;
  readDHTtemp(&tempFloat);
  string = "TEMPERATURE ";
  stringToPrint = string + tempFloat;
  serStr(stringToPrint);
}

// DHT Temperature
void readDHTtemp(float *a) {
  float t = dht.readTemperature();
  *a = t;
}
 
// DHT Humidity
void readDHThum(float *a) {
  float h = dht.readHumidity();
  *a = h;
}

// Send string to serial monitor with millis() counter and date/time
void serStr(String serString) {
  long millisTime = millis();
  String delimiter = "|";
  String stringToPrint = millisTime + delimiter + serString;
  Serial.println(stringToPrint);
}
